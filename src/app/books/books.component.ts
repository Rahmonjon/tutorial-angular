import { Component, OnInit } from '@angular/core';

interface Book{
  name: string;
  auther: string;
  image: string;
}


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
 
books: Book[] = [
  {
    name: 'Clean Code',
    auther: 'Robert C Martin',
    image: 'https://m.media-amazon.com/images/I/41xShlnTZTL._SX376_BO1,204,203,200_.jpg',
  },
  {
    name: 'Pragmatic Programmer',
    auther: 'David Thomas',
    image: 'https://m.media-amazon.com/images/I/51W1sBPO7tL.jpg',
  }
]
  // name: string = 'Clean Code';
  // auther: string = 'Robert C Martin';
  // src: string = 'https://m.media-amazon.com/images/I/41xShlnTZTL._SX376_BO1,204,203,200_.jpg';

  // name2: string = 'Pragmatic Programmer'
  // auther2: string = 'David Thomas';
  // src2: string = 'https://m.media-amazon.com/images/I/51W1sBPO7tL.jpg';

isShowing: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }
// commented simply cause of ngIf directives
  // isDisabled: boolean = false;
  // handleClick(){
  //   this.isDisabled = true;
  //   // alert('I am working')
  // }

  // myName: string = '';

  // toggleBooks(){
  //   // if isShowing is true, then make it false
  //   // or isShowing is false, then make it true
  //   this.isShowing = !this.isShowing;
  // }
 
}
